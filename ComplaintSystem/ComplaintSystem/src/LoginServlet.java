

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import java.sql.*;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	public String un,ps;
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		un="Admin@gmail.com";
		ps="Admin123";
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		
		String u=request.getParameter("Uname");
		String p=request.getParameter("pswd");
		
		if(u.equals(un) && p.equals(ps))
		{
				RequestDispatcher rd=request.getRequestDispatcher("/Complaint.html");
				rd.forward(request,response);
		}
		else
		{
			pw.print("Invalid user");
			RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
			rd.include(request,response);
		}
	}
		/*try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Java","root","root");
			String q="select uname,password from users";
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(q);
			
			while(rs.next())
			{
				if(u.equals(rs.getString(1)) && p.equals(rs.getString(2)))
				{
					RequestDispatcher rd=request.getRequestDispatcher("/Complaint.html");
					rd.forward(request,response);
					cnt=1;
				}
			}
			if(cnt==0)
			{
				pw.print("Invalid user");
				RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
				rd.include( request,response);
			}
			con.close();
		}
		catch(Exception e)
		{
			pw.println("Error!!"+e);
		}
	}*/

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
