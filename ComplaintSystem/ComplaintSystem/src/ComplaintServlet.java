

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Servlet implementation class ComplaintServlet
 */
@WebServlet("/ComplaintServlet")
public class ComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		
		String t=request.getParameter("type");
		String d=request.getParameter("details");
		String l=request.getParameter("location");
		String b=request.getParameter("complaint_by");
		
		LocalDateTime localDate=LocalDateTime.now();
		String date=DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);

		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Java","root","root");
			PreparedStatement pst=con.prepareStatement("insert into complaint_master(type,details,location,complaint_by,c_date,status) values(?,?,?,?,?,?)");
			pst.setString(1,t);
			pst.setString(2,d);
			pst.setString(3,l);
			pst.setString(4,b);
			pst.setString(5,date);
			pst.setString(6,"A");
			
			int i=pst.executeUpdate();
			if(i==1)
			{
				pw.println("Insert successful");
				
				pw.println("<form action='CompletedComplain' method='post'");
				Statement st=con.createStatement();
				ResultSet rs=st.executeQuery("select * from complaint_master");
				while(rs.next())
				{
					pw.println("<table border=1>"
							+	"<tr>"
							+	"<td>" + rs.getString(2) + "</td>"
							+	"<td>" + rs.getString(3) + "</td>"
							+	"<td>" + rs.getString(4) + "</td>"
							+	"<td>" + rs.getString(5) + "</td>"
							+	"<td>" + rs.getDate(6) + "</td>"
							+	"<td>" + rs.getString(7) + "</td>"
							+	"</tr>"
							+	"</table>");
				}
				pw.println("<input type='Submit' value='Change Status'>");
			}
			else
			{
				pw.print("Error!!");
			}
			con.close();
		}
		catch(Exception e)
		{
			pw.println("Error!");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
